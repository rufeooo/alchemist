import pygame
import math
import random

# key_stroke_I time_i and end_time_i follow the same rules as a python range
# that is: [start, end) inclusive, exclusive
class KeyStroke:
  def __init__(self):
    self.action_id_i = None
    self.time_i = 0
    self.end_time_i = None
    
  def __repr__(self):
    return "action %d. (%d-%d=%d)" % (self.action_id_i,
      self.end_time_i,
      self.time_i,
      self.end_time_i - self.time_i)

class MouseStroke:
  def __init__(self):
    self.button_i = None
    self.source_x_i = 0
    self.source_y_i = 0
    self.source_time_i = 0
    self.dest_x_i = 0
    self.dest_y_i = 0
    self.dest_time_i = 0
    
class GlobalSettings:
  def __init__(self):
    with open('grid_size.txt', 'r') as f:
      self.cells = eval(f.read())
    self.grid_width_i = 128
    self.grid_height_i = 128
    self.resolution_T = (self.grid_width_i*self.cells[0]+128, self.grid_height_i*self.cells[1] + 128)
   
  def grid_rect(self, x_i, y_i):
    rect_I = pygame.Rect((x_i * self.grid_width_i, y_i * self.grid_height_i),
      (self.grid_width_i, self.grid_height_i))
    return rect_I
    
class WorldState:
  def __init__(self):
    self.world_state_DT = {}
    with open('deck.txt', 'r') as f:
      self.deck_Ls = list(''.join(eval(f.read())))
    with open('points.txt', 'r') as f:
      self.points_Di = eval(f.read())
    self.inventory_Ls = ''
    self.inventory_size_i = 5
    self.score_i = 0
    self.failures_Ls = ['BC', 'CD', 'DE']
    self.sour_cells_ST = set()
    
  def generate_default_inventory(self):
    self.inventory_size_i = len('BCDEG')
    return 'BCDEG'
    
class GlobalFont:
  def __init__(self):
    self.font = pygame.font.Font(pygame.font.get_default_font(), 32)
    
  def render_text(self, text_s):
    return self.font.render(text_s, True, (255, 0, 0))
  
  def size_text(self, text_s):
    return self.font.size(text_s)
    
class SystemEvent:
  def __init__(self):
    self.event_s = ''
    
class Harmony:
  FrameDurationMs = 75
  DisharmonyFrameDurationMs = 33
  def __init__(self):
    self.harmony_time_i = 0
    self.origin_T = (0,0)
    self.discord_b = False
    self.blue_b = False
    
class ScoreEvent:
  Exponent = .6
  Duration_ms = 1600
  def __init__(self):
    self.score_time_i = 0
    self.score_value_i = 0
    
  def is_complete(self, elapsed_i):
    return (elapsed_i - self.score_time_i) > ScoreEvent.Duration_ms
    
  def get_value(self, elapsed_i):
    offset_i = elapsed_i - self.score_time_i
    completed_f = float(offset_i) / ScoreEvent.Duration_ms
    percent_f = math.pow(completed_f, ScoreEvent.Exponent)
    return int(percent_f * self.score_value_i)
    
class Replay:
  def __init__(self):
    self.start_time_i = 0
    self.played_Li = []
