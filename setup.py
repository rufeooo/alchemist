import sys
from cx_Freeze import setup, Executable
import glob, os
import pygame

# GUI applications require a different base on Windows (the default is for a
# console application).
base = None
if sys.platform == "win32":
    base = "Win32GUI"

def AddFonts(files):
  PYGAMEDIR = os.path.split(pygame.base.__file__)[0]
  SDL_FONTS = ["freesansbold.ttf"]
          
  for f in SDL_FONTS:
    srcPath = os.path.join(PYGAMEDIR, f)
    files.append((srcPath, f))
  
extra_files = []
extra_files.extend(glob.glob(os.path.join('images','*.png')))
extra_files.extend(glob.glob(os.path.join('sounds','*.wav')))
extra_files.extend(['deck.txt', 'points.txt', 'grid_size.txt'])
extra_files = zip(extra_files, extra_files)
AddFonts(extra_files)
# Exit(1)

MODULE_EXCLUDES = [
'doctest',
'pdb',
'unittest',
'inspect',
'email',
'AppKit',
'Foundation',
'bdb',
'difflib',
'tcl',
'Tkinter',
'Tkconstants',
'curses',
'setuptools',
'BaseHTTPServer',
'_LWPCookieJar',
'_MozillaCookieJar',
'ftplib',
'gopherlib',
'_ssl',
'tty',
'webbrowser',
'base64',
'compiler',
'pydoc']

# Dependencies are automatically detected, but it might need fine tuning.
build_exe_options = {"packages": ["os"], "excludes": MODULE_EXCLUDES, 'include_files':extra_files}

setup(  name = "Harmony",
        version = "1.0",
        description = "Global game jam 2015!",
        options = {"build_exe": build_exe_options},
        executables = [Executable("Harmony.py", base=base)])

