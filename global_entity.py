import component as c
import collections

def EmptyDictFn():
  return {}
  
_all_entity_component_DDI = collections.defaultdict(EmptyDictFn)
_start_local_id = 1000
_next_i = _start_local_id
_game_surface = None
_overlay_surface = None

def create():
  global _next_i
  return_i = _next_i
  _next_i += 1
  return return_i
  
def add_component(entity_i, component_I):
  global _all_entity_component_DDI, _start_local_id
  # print 'adding %s component to %d' % (str(component_I.__class__), entity_i)
  _all_entity_component_DDI[component_I.__class__][entity_i] = component_I
  
def remove_component(entity_i, cls_C):
  global _all_entity_component_DDI
  try:
    del _all_entity_component_DDI[cls_C][entity_i]
  except KeyError:
    return False
  return True
  
def aspect(cls_C):
  global _all_entity_component_DDI
  return _all_entity_component_DDI[cls_C].items()

def iterate_aspect(cls_C):
  global _all_entity_component_DDI
  return _all_entity_component_DDI[cls_C].iteritems()
  
def count_aspect(cls_C):
  global _all_entity_component_DDI
  return len(_all_entity_component_DDI[cls_C].keys())
  
def delete(entity_i, debug_b=False):
  global _all_entity_component_DDI
  deletion_b = False
  if debug_b:
    print 'deleting entity %d' % entity_i
  for cls_C, component_DI in _all_entity_component_DDI.iteritems():
    if entity_i in component_DI:
      del component_DI[entity_i]
      deletion_b = True
      if debug_b:
        print 'class removed %s' % str(cls_C)
        print '%d elements remain' % len(component_DI.keys())
  return deletion_b
  
def delete_all(cls_C):
  global _all_entity_component_DDI
  del _all_entity_component_DDI[cls_C]
  
def find(entity_i, cls_C):
  global _all_entity_component_DDI
  # print 'find request on %s for entity %d' % (str(cls_C), entity_i)
  component_I = None
  try:
    component_I = _all_entity_component_DDI[cls_C][entity_i]
  except KeyError:
    pass
  return component_I
  
def get_count():
  global _all_entity_component_DDI
  component_sum_i = 0
  class_sum_i = 0
  for cls_C, component_DI in _all_entity_component_DDI.iteritems():
    class_sum_i += 1
    component_sum_i += len(component_DI.keys())
    
  return (class_sum_i, component_sum_i)
  
def set_game_surface(surface_I):
  global _game_surface
  _game_surface = surface_I
  
def get_game_surface():
  global _game_surface
  return _game_surface
  
def set_overlay_surface(surface_I):
  global _overlay_surface
  _overlay_surface = surface_I
  
def get_overlay_surface():
  global _overlay_surface
  return _overlay_surface
  
def init_globals():
  sys_ent_i = create()
  se_I = c.SystemEvent()
  add_component(sys_ent_i, se_I)
  
  settings_ent_i = create()
  gs_I = c.GlobalSettings()
  add_component(settings_ent_i, gs_I)
  
  world_ent_i = create()
  ws_I = c.WorldState()
  add_component(world_ent_i, ws_I)
  
  font_ent_i = create()
  font_I = c.GlobalFont()
  add_component(font_ent_i, font_I)
  
def get_system_event():
  return aspect(c.SystemEvent)[0][1]
    
def get_global_settings():
  return aspect(c.GlobalSettings)[0][1]
  
def get_world_state():
  return aspect(c.WorldState)[0][1]
  
def get_font():
  return aspect(c.GlobalFont)[0][1]
  