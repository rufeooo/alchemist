import types

def all_submodules(module, reloaded_set):
  if module not in reloaded_set:
    reloaded_set.add(module)
    print 'reloading %s' % module.__name__
    reload(module)
    for attr in module.__dict__.values():
      if type(attr) == types.ModuleType:
        all_submodules(attr, reloaded_set)