import core
import cProfile
import pygame
import recursive_reload
import persist

def main():
  core.onetime_init()
  core.startup()
  while True:
    event = core.get_event()
    if event == 'reload':
      core.shutdown()
      recursive_reload.all_submodules(core, set([pygame, cProfile, persist]))
      core.startup()
    elif event == 'exit':
      core.shutdown()
      break
    pr = cProfile.Profile()
    pr.enable()
    start_i = pygame.time.get_ticks()
    core.update()
    stop_i = pygame.time.get_ticks()
    pr.disable()
  core.onetime_teardown()
  
if __name__ == "__main__":
  main()
  