import pygame
import sys_input
import global_entity
import component as c
import random
import persist

global _sounds_DI
_sounds_DI = {}
global _images_DI, _small_images_DI
_images_DI = {}
_small_images_DI = {}
global inventory_image_I
inventory_image_I = None
global _harmony_animation
_harmony_animation = []
global _blue_harmony_animation
_blue_harmony_animation = []
global _disharmony_animation
_disharmony_animation = []
global _sour_note_I
_sour_note_I = None
global _X_I
_X_I = None
global repeat_image_I
repeat_image_I = None
global coda_image_I
coda_image_I = None

def onetime_init():
  pygame.init()
  pygame.display.set_caption('Harmony')
  
def onetime_teardown():
  pygame.quit()
  
def startup():
  global_entity.init_globals()
  
  gs_I = global_entity.get_global_settings()
  pygame.display.set_mode(gs_I.resolution_T, pygame.DOUBLEBUF | pygame.HWSURFACE)
  
  global_entity.set_game_surface(pygame.display.get_surface())
  global_entity.set_overlay_surface(pygame.Surface(gs_I.resolution_T, pygame.SRCALPHA))
  
  # for now, random init
  # _random_init()
  
  _sound_init()
  _image_init()
  
  font_I = global_entity.get_font()
  global _X_I
  _X_I = font_I.render_text('X')
  
  ws_I = global_entity.get_world_state()
  random.shuffle(ws_I.deck_Ls)
  
  if len(persist.note_history):
    replay_I = c.Replay()
    replay_I.start_time_i = pygame.time.get_ticks()
    global_entity.add_component(global_entity.create(), replay_I)  
  
def shutdown():
  pass
  
def _failure_check(ws_I, notes_s):
  for failure_s in ws_I.failures_Ls:
    if failure_s in notes_s:
      return True
      
  return False
  
def score_notes(ws_I, notes_s):
  if _failure_check(ws_I, notes_s):
    print 'sour note! ', notes_s
    return -100
    
  if notes_s in ws_I.points_Di:
    return ws_I.points_Di[notes_s]
  
  raise Exception('unhandled note' + notes_s)
    
def _spawn_score(value_i):
  score_ent_i = global_entity.create()
  score_I = c.ScoreEvent()
  score_I.score_time_i = pygame.time.get_ticks()
  score_I.score_value_i = value_i
  global_entity.add_component(score_ent_i, score_I)
  
def adjacency_check(ws_I, cell_T, rect_I, type_s, adjacent_cells_LT):
  # print cell_T, 'adjacency check type: ', type_s
  test_offsets_LT = [(-1,0), (1, 0), (0, -1), (0, 1)]
  adjacent_render_locations_LF = [lambda r: r.midleft,lambda r: r.midright, lambda r: r.midtop, lambda r: r.midbottom]
  result_Ss = set(type_s)
  for offset_T, adjacent_F in zip(test_offsets_LT, adjacent_render_locations_LF):
    current_T = (cell_T[0] + offset_T[0], cell_T[1] + offset_T[1])
    # print '  search cell', current_T
    if current_T in ws_I.world_state_DT:
      # print 'interact ', type_s, ' with', world_state_DT[current_T]
      result_Ss.add(ws_I.world_state_DT[current_T])
      render_loc_T = adjacent_F(rect_I)
      adjacent_cells_LT.append(render_loc_T)
  unique_adjacent_s = ''.join(sorted(result_Ss))
  print 'musical sound', unique_adjacent_s
  return unique_adjacent_s
  
def update():
  global _sounds_DI, _harmony_animation, _disharmony_animation
  global inventory_image_I, _images_DI, _small_images_DI
  global _sour_note_I
  global repeat_image_I, coda_image_I
  
  gs_I = global_entity.get_global_settings()
  ws_I = global_entity.get_world_state()
  font_I = global_entity.get_font()
  surface_I = global_entity.get_game_surface()
  overlay_I = global_entity.get_overlay_surface()
    
  mouse_T = pygame.mouse.get_pos()
  for y_i in xrange(0, gs_I.cells[1]):
    for x_i in xrange(0, gs_I.cells[0]):
      cell_T = (x_i, y_i)
      rect_I = gs_I.grid_rect(x_i, y_i)
      if rect_I.collidepoint(mouse_T):
        pygame.draw.rect(overlay_I, (225, 246, 232, 64), rect_I)
      pygame.draw.rect(surface_I, (225, 246, 232), rect_I, 2)
      if cell_T in ws_I.world_state_DT:
        cell_type_s = ws_I.world_state_DT[cell_T]
        text_surface_I = _images_DI[cell_type_s]
        #text_surface_I = font_I.render_text(cell_type_s) # render symbol
        dest_I = text_surface_I.get_rect()
        dest_I.center = rect_I.center
        surface_I.blit(text_surface_I, dest_I)  
      else:
        # Only processing input when on an empty cell
        if rect_I.collidepoint(mouse_T):
          for entity_i, key_stroke_I in global_entity.aspect(c.KeyStroke):
            if key_stroke_I.action_id_i >= ord('A') and key_stroke_I.action_id_i <= ord('z'):
              cell_type_s = chr(key_stroke_I.action_id_i).upper()
              if cell_type_s in ws_I.inventory_Ls:
                adjacent_cells_LT = []
                # print cell_type_s
                chord_s = adjacency_check(ws_I, cell_T, rect_I, cell_type_s, adjacent_cells_LT)
                if len(chord_s) <= 3:
                  key_stroke_I.end_time_i = pygame.time.get_ticks()
                  failure_b = _failure_check(ws_I, chord_s)
                  spawn_time_i = pygame.time.get_ticks()
                  is_blue_b = len(chord_s) == 3
                  harmony_I = _spawn_harmony(spawn_time_i, mouse_T, is_blue_b, failure_b)
                  
                  max_time_ms_i = 0
                  if failure_b:
                    max_time_ms_i = 250
                    _sour_note_I.play(0)
                    pygame.mixer.fadeout(250)
                    for location_T in adjacent_cells_LT:
                      ws_I.sour_cells_ST.add(location_T)
                  else:
                    persist.note_history.append(chord_s)
                    persist.location_history.append(cell_T)
                    persist.cell_type_history.append(cell_type_s)
                    for index_i in xrange(0, len(chord_s)-1):
                      spawn_time_i += 150
                      harmony_I = _spawn_harmony(spawn_time_i, mouse_T, is_blue_b, failure_b)
                  for note_s in chord_s:
                    _sounds_DI[note_s].play(0, max_time_ms_i)
                  
                  # print 'assignment of ', cell_type_s, ' to location ',  cell_T
                  ws_I.inventory_Ls = ws_I.inventory_Ls.replace(cell_type_s, '', 1)
                  ws_I.inventory_Ls = ''.join([ws_I.inventory_Ls, ws_I.deck_Ls.pop()])
                  ws_I.world_state_DT[cell_T] = cell_type_s
                  score_i = score_notes(ws_I, chord_s)
                  _spawn_score(score_i)
                else:
                  pygame.draw.line(overlay_I, (255, 0, 0, 255), rect_I.bottomleft, rect_I.topright, 5)
                  pygame.draw.line(overlay_I, (255, 0, 0, 255), rect_I.topleft, rect_I.bottomright, 5)
    
  # animations
  for harmony_ent_i, harmony_I in global_entity.iterate_aspect(c.Harmony):
    offset_i = pygame.time.get_ticks() - harmony_I.harmony_time_i
    if harmony_I.discord_b:
      _render_harmony(offset_i, c.Harmony.DisharmonyFrameDurationMs, surface_I, harmony_I.origin_T, _disharmony_animation)
    elif harmony_I.blue_b:
      _render_harmony(offset_i, c.Harmony.FrameDurationMs, surface_I, harmony_I.origin_T, _blue_harmony_animation)
    else:
      _render_harmony(offset_i, c.Harmony.FrameDurationMs, surface_I, harmony_I.origin_T, _harmony_animation)
  
  # render inventory notes
  inven_rect_I = inventory_image_I.get_rect()
  inven_rect_I.centery = surface_I.get_rect().bottom - 64
  inven_rect_I.centerx = surface_I.get_rect().centerx
  surface_I.blit(inventory_image_I, inven_rect_I)
  note_x_loc = inven_rect_I.left+150
  note_y_loc_offset = {'G':75, 'B':50 , 'C':38, 'D':20, 'E':8}
  for note_s in ws_I.inventory_Ls:
    inven_note_rect_I = _small_images_DI[note_s].get_rect()
    note_y_loc = inven_rect_I.top + note_y_loc_offset[note_s]
    inven_note_rect_I.center = (note_x_loc, note_y_loc)
    surface_I.blit(_small_images_DI[note_s], inven_note_rect_I)
    note_x_loc = note_x_loc + 75
    
  # render playback button
  repeat_rect_I = repeat_image_I.get_rect()
  repeat_rect_I.left = surface_I.get_rect().left + 12
  repeat_rect_I.bottom = surface_I.get_rect().bottom
  surface_I.blit(repeat_image_I, repeat_rect_I)
  if repeat_rect_I.collidepoint(mouse_T):
    pygame.draw.rect(overlay_I, (173, 216, 217, 64), repeat_rect_I)
  
      
  # render coda
  coda_rect_I = coda_image_I.get_rect()
  coda_rect_I.right = surface_I.get_rect().right - 128
  coda_rect_I.centery = surface_I.get_rect().bottom - 64
  surface_I.blit(coda_image_I, coda_rect_I)
  if coda_rect_I.collidepoint(mouse_T):
    pygame.draw.rect(overlay_I, (173, 216, 217, 64), coda_rect_I)
  
  # test mouse input
  for mouse_ent_i, mouse_stroke_I in global_entity.iterate_aspect(c.MouseStroke):
    if repeat_rect_I.collidepoint((mouse_stroke_I.source_x_i, mouse_stroke_I.source_y_i)):
      se_I = global_entity.get_system_event()
      se_I.event_s = 'reload'
    elif coda_rect_I.collidepoint((mouse_stroke_I.source_x_i, mouse_stroke_I.source_y_i)):
      persist.note_history = []
      persist.location_history = []
      persist.cell_type_history = []
      se_I = global_entity.get_system_event()
      se_I.event_s = 'reload'
      
  # replay playback
  for replay_ent_i, replay_I in global_entity.iterate_aspect(c.Replay):
    now_i = pygame.time.get_ticks()
    offset_i = now_i - replay_I.start_time_i
    frame_i = offset_i / 600
    for index_i in xrange(len(replay_I.played_Li), min(frame_i, len(persist.note_history))):
      replay_I.played_Li.append(index_i)
      chord_s = persist.note_history[index_i]
      cell_type_s = persist.cell_type_history[index_i]
      cell_T = persist.location_history[index_i]
      ws_I.world_state_DT[cell_T] = cell_type_s
      rect_I = gs_I.grid_rect(*cell_T)
      is_blue_b = len(chord_s) == 3
      _spawn_harmony(now_i, rect_I.center, is_blue_b, False)
      for index_i in xrange(0, len(chord_s)-1):
        now_i += 150
        harmony_I = _spawn_harmony(now_i, rect_I.center, is_blue_b, False)
      print 'replay frame', index_i, ' chord ', chord_s
      for note_s in chord_s:
        _sounds_DI[note_s].play(0)
        
  # inventory_surface_I = font_I.render_text(ws_I.inventory_Ls)
  # inventory_rect_I = inventory_surface_I.get_rect()
  # inventory_rect_I.centerx = surface_I.get_rect().centerx
  # inventory_rect_I.centery = surface_I.get_rect().bottom - 64
  # overlay_I.blit(inventory_surface_I, inventory_rect_I)
  
  # Remove fully accumulated scores
  elapsed_i = pygame.time.get_ticks()
  score_LT = global_entity.aspect(c.ScoreEvent)
  for score_ent_i, score_I in score_LT:
    if score_I.is_complete(elapsed_i):
      ws_I.score_i += score_I.score_value_i
      global_entity.delete(score_ent_i)
      
  # Accumulate partial scores
  score_i = ws_I.score_i
  for score_ent_i, score_I in global_entity.iterate_aspect(c.ScoreEvent):
    score_i += score_I.get_value(elapsed_i)
    
  for location_T in ws_I.sour_cells_ST:
    X_rect_I = _X_I.get_rect()
    X_rect_I.center = location_T
    surface_I.blit(_X_I, X_rect_I)
    
  # render score
  score_surface_I = font_I.render_text(str(score_i))
  score_rect_I = score_surface_I.get_rect()
  score_rect_I.right = surface_I.get_rect().right
  score_rect_I.top = surface_I.get_rect().top
  overlay_I.blit(score_surface_I, score_rect_I)
  
  if len(ws_I.inventory_Ls) == 0:
    # ws_I.inventory_Ls = ws_I.generate_default_inventory()
    for x_i in xrange(0, ws_I.inventory_size_i):
      ws_I.inventory_Ls = ''.join([ws_I.inventory_Ls, ws_I.deck_Ls.pop()])
    
  sys_input.update(pygame.time.get_ticks())
  surface_I.blit(overlay_I, (0,0))
  pygame.display.flip()
  surface_I.fill((0,0,0))
  overlay_I.fill((0, 0, 0, 0))
  
def get_event():
  se_I = global_entity.get_system_event()
  event_s = se_I.event_s
  se_I.event_s = ''
  return event_s
  
def _random_init():
  gs_I = global_entity.get_global_settings()
  ws_I = global_entity.get_world_state()
  charlist_s = "BCDEG"
  for y_i in xrange(0, gs_I.cells[1]):
    for x_i in xrange(0, gs_I.cells[0]):
      cell_T = (x_i, y_i)
      char_i = random.randint(0, len(charlist_s)-1)
      ws_I.world_state_DT[cell_T] = charlist_s[char_i:char_i+1]
  
def _sound_init():
  global _sounds_DI, _sour_note_I
  _sour_note_I = pygame.mixer.Sound("sounds/sour.wav")
  file_format_s = "sounds/sound%s.wav"
  for note_s in "BCDEG":
    filename_s = file_format_s % note_s
    _sounds_DI[note_s] = pygame.mixer.Sound(filename_s)
    
def _image_init():
  global _images_DI, inventory_image_I, repeat_image_I, coda_image_I
  inventory_image_I = pygame.image.load("images/inventory.png")
  repeat_image_I = pygame.image.load("images/repeat.png")
  coda_image_I = pygame.image.load("images/coda.png")
  _load_note_images("images/%s.png", _images_DI)
  _load_note_images("images/small%s.png", _small_images_DI)
  
  _animation_init()
    
def _load_note_images(name_format_s, images_DI):
  for note_s in "BCDEG":
    filename_s = name_format_s % note_s
    images_DI[note_s] = pygame.image.load(filename_s)
    
def _animation_init():
  global _harmony_animation, _disharmony_animation
  _load_anim("images/Chord%d.png", _harmony_animation)
  _load_anim("images/bad%d.png", _disharmony_animation)
  _load_anim("images/bChord%d.png", _blue_harmony_animation)
  _disharmony_animation = _disharmony_animation[:5]
  
def _load_anim(name_format_s, image_LI):
  for index_i in xrange(1, 10):
    filename_s = name_format_s % index_i
    # print 'loading chord', filename_s
    image_LI.append(pygame.image.load(filename_s))
  
def _spawn_harmony(time_i, origin_T, is_blue_b, failure_b):
  # print 'spawn harmony time', time_i, 'origin', origin_T
  ent_i = global_entity.create()
  harmony_I = c.Harmony()
  harmony_I.harmony_time_i = time_i
  harmony_I.origin_T = origin_T
  harmony_I.blue_b = is_blue_b
  harmony_I.discord_b = failure_b
  global_entity.add_component(ent_i, harmony_I)
  return harmony_I
  
def _render_harmony(offset_i, duration_i, surface_I, location_T, animation_LI):
  if offset_i > 0 and offset_i < len(animation_LI)*duration_i:
    frame_i = (offset_i / duration_i)
    # print 'frame ', frame_i
    harmony_rect_I = animation_LI[frame_i].get_rect()
    harmony_rect_I.center = location_T
    surface_I.blit(animation_LI[frame_i], harmony_rect_I)