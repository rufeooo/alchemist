import pygame
import global_entity
import component as c

def update(elapsed_i):
  se_I = global_entity.get_system_event()
  # Purge mouse strokes that have had a frame to be processed
  mouseStroke_LT = global_entity.aspect(c.MouseStroke)
  for mouseStroke_T in mouseStroke_LT:
    if mouseStroke_T[1].dest_time_i:
      # print( 'purging mouse stroke' )
      global_entity.delete(mouseStroke_T[0])
  keystroke_LT = global_entity.aspect(c.KeyStroke)
  for key_ent_i, keystroke_I in keystroke_LT:
    if keystroke_I.end_time_i:
      global_entity.delete(key_ent_i)
      
  events = pygame.event.get()
  for e in events:
    if e.type == pygame.QUIT:
      se_I.event_s = 'exit'
    elif e.type == pygame.MOUSEBUTTONDOWN:
      pixel = pygame.display.get_surface().get_at(e.pos)
      # print( 'Pixel color at (%d, %d): %dr %dg %db' % (e.pos[0], e.pos[1], pixel.r, pixel.g, pixel.b) )
      mouse_stroke_entity_i = global_entity.create()
      stroke_I = c.MouseStroke()
      stroke_I.button_i = e.button
      stroke_I.source_x_i = e.pos[0]
      stroke_I.source_y_i = e.pos[1]
      stroke_I.source_time_i = elapsed_i
      global_entity.add_component(mouse_stroke_entity_i, stroke_I)
    elif e.type == pygame.MOUSEBUTTONUP: 
      for mouseStroke_T in global_entity.iterate_aspect(c.MouseStroke):
        if mouseStroke_T[1].button_i == e.button:
          mouseStroke_T[1].dest_x_i = e.pos[0]
          mouseStroke_T[1].dest_y_i = e.pos[1]
          mouseStroke_T[1].dest_time_i = elapsed_i
          break
    elif e.type == pygame.KEYDOWN:
      if (e.key == pygame.K_F5):
        se_I.event_s = 'reload'
      else:
        key_i = global_entity.create()
        stroke_I = c.KeyStroke()
        stroke_I.action_id_i = e.key
        stroke_I.time_i = elapsed_i
        global_entity.add_component(key_i, stroke_I)
        # print( 'keystroke created', stroke_I.action_id_i, ' Entity %d' % key_i )
    elif e.type == pygame.KEYUP:
      action_id_i = e.key
      matched = False
      for entity_i, key_stroke_I in global_entity.iterate_aspect(c.KeyStroke):
        # print( entity_i, key_stroke_I.action_id_i, "iterating keystroke" )
        if key_stroke_I.action_id_i == action_id_i and key_stroke_I.end_time_i is None:
          key_stroke_I.end_time_i = elapsed_i
          # print( 'local keyup', key_stroke_I )
          matched = True
      if not matched:
        pass
        # print('<<<unmatched key up>>> key: ', e.key)
        